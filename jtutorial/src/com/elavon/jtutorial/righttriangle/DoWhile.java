package com.elavon.jtutorial.righttriangle;

public class DoWhile {

	public static void main(String[] args) {
		int ctr = 0;
		int x = 0;
		
		do{
			System.out.print("*");
			for(x=0;x<ctr;x++){
				System.out.print("*");
			}
			ctr++;
			System.out.println();
		}while(ctr<10);

	}

}
