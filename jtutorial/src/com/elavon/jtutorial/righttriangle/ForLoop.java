package com.elavon.jtutorial.righttriangle;

public class ForLoop {

	static void RightTriangle(int height) {

		for (int ctr = 1; ctr <= height; ctr++) {

			for (int x = 1; x <= ctr; x++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
	
	static void InvertedRightTriangle (int height){
		
		for (int ctr = height ; ctr >= 1 ; ctr--) {

			for (int x = 1; x <= ctr; x++) {
				System.out.print("*");
			}
			System.out.println();
		}
	
	}
	
	static void HalfDiamond(int height){
		ForLoop.RightTriangle(height);
		ForLoop.InvertedRightTriangle(height-1);
	}

	public static void main(String[] args) {

		ForLoop.RightTriangle(10);
		ForLoop.InvertedRightTriangle(10);
		ForLoop.HalfDiamond(10);

	}

}
