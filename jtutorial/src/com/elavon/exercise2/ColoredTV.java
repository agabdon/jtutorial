package com.elavon.exercise2;


public class ColoredTV extends TV {

	int brightness;
	int contrast;
	int picture;

	ColoredTV(String brand, String model) {
		super(brand, model);
		brightness = 50;
		contrast = 50;
		picture = 50;

		// TODO Auto-generated constructor stub
	}

	int brightnessUp() {
		brightness = brightness + 1;
		return brightness;

	}
	
	int brightnessDown() {
		brightness = brightness - 1;
		return brightness;

	}
	
	int contrastUp() {
		contrast = contrast + 1;
		return contrast;

	}
	
	int contrastDown() {
		contrast = contrast - 1;
		return contrast;

	}
	
	int pictureUp() {
		picture = picture + 1;
		return picture;

	}
	
	int pictureDown() {
		picture = picture - 1;
		return picture;

	}
	
	 void switchToChannel(int channel){
		this.channel = channel;

	}
	 
	void mute(){
		this.volume = 0;
	}
	
	public String toString() {

		return brand + " " + model + "[ on:" + powerOn + ", " + "channel:"
				+ channel + ", volume:" + volume + " ] [ b:" + brightness + ", c:" + contrast + ", p:" + picture + "]" ;

	}

	public static void main(String[] args) {
		
		TV bnwTV = new TV("Admiral", "modelA1");
		TV sonyTV = new ColoredTV("SONY", "model S1");
		ColoredTV sharpTV = new ColoredTV("SHARP", "model SH1");
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		
		//sonyTV.brightnessUp();
		
		//sharpTV.turnOn();
		sharpTV.mute();

		sharpTV.brightnessDown();
		sharpTV.brightnessDown();
		sharpTV.brightnessDown();
		sharpTV.brightnessDown();
		
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		sharpTV.contrastUp();
		
		sharpTV.pictureDown();
		sharpTV.pictureDown();
		sharpTV.pictureDown();
		sharpTV.pictureDown();
		sharpTV.pictureDown();
		sharpTV.pictureDown();
		
		System.out.println(sharpTV);

	}

}
