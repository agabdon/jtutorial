package com.elavon.exercise2;

public interface NetworkConnection {
	
	String connect();
	boolean connectionStatus();

}
