package com.elavon.exercise2;

public class TV {

	protected String brand;
	protected String model;
	protected boolean powerOn;
	protected int channel;
	protected int volume;

	TV(String brand, String model) {
		this.brand = brand;
		this.model = model;
		powerOn = false;
		channel = 0;
		volume = 5;
	}

	boolean turnOn() {

		this.powerOn = true;
		return powerOn;

	}

	boolean turnOff() {

		this.powerOn = false;
		return powerOn;

	}

	int channelUp() {

		this.channel = channel + 1;
		return channel;

	}

	int channelDown() {

		this.channel = channel - 1;
		return channel;

	}

	int volumeUp() {

		this.volume = volume + 1;
		return volume;

	}

	int volumeDown() {
		this.volume = volume - 1;
		return volume;

	}

	public String toString() {

		return brand + " " + model + "[ on:" + powerOn + ", " + "channel:"
				+ channel + ", volume:" + volume + " ]";

	}

	public static void main(String[] args) {
		int chUp = 5;
		int chDown = 1;
		int volUp = 1;
		int volDown = 3;

		TV tv1 = new TV("Andre Electronics", "ONE");

		System.out.println(tv1.toString());
		
		tv1.turnOn();

		int i = 0;
		for (i = 0; i < chUp; i++) {
			tv1.channelUp();
		}

		for (i = chDown; i > 0; i--) {
			tv1.channelDown();
		}

		for (i = volDown; i > 0; i--) {
			tv1.volumeDown();
		}

		for (i = 0; i < volUp; i++) {
			tv1.volumeUp();
		}
		
		tv1.turnOff();

		System.out.println(tv1.toString());

	}

}
