package com.elavon.exercise2;

public class SmartTV extends ColoredTV implements NetworkConnection {

	String network = " ";

	SmartTV(String brand, String model) {
		super(brand, model);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String connect() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean connectionStatus() {
		// TODO Auto-generated method stub
		return false;
	}

	void connectToNetwork(String network) {
		this.network = network;

	}

	void hasNetworkConnection() {

	}

	public String toString() {

		return brand + " " + model + "[ on:" + powerOn + ", " + "channel:"
				+ channel + ", volume:" + volume + " ] [ b:" + brightness
				+ ", c:" + contrast + ", p:" + picture
				+ "] [ network connection type:" + network + " ]";

	}

	public static void main(String[] args) {
		SmartTV samsungTV = new SmartTV("SAMSUNG", "SM TV1");

		System.out.println(samsungTV);

		samsungTV.mute();

		samsungTV.brightnessUp();
		samsungTV.brightnessUp();
		samsungTV.brightnessUp();

		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();
		samsungTV.brightnessDown();

		samsungTV.contrastUp();
		samsungTV.contrastUp();
		samsungTV.contrastUp();

		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.contrastDown();
		samsungTV.contrastDown();

		samsungTV.pictureUp();
		samsungTV.pictureUp();
		samsungTV.pictureUp();

		samsungTV.pictureDown();

		samsungTV.channelUp();
		samsungTV.channelUp();
		
		samsungTV.connectToNetwork("TestNetwork");

		System.out.println(samsungTV);

		
	}

}
